Exigo SQL Repository
	SQL1-Model-Database
	   Contains additional objects to complement the base Microsoft SQL Server model database.

	SQL1-SystemLog-Database
	   Contains objects and data for the base SystemLog database.

	SQL1-SQLDBA
	   Contains objects used by Visalus database administrator for server/database reporting and
	   maintenance.